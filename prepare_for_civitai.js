// @ts-check
/**
 * @typedef {import("./ui").PromptFn} PromptFn
 */
/**
 * @typedef {import("./ui").LogFn} LogFn
 */

const fs = require("fs/promises");
const { exec } = require("child_process");
const path = require("path");
const {
  initUI,
  rewriteConsole,
  stopInteractivity,
  progressbar,
} = require("./ui");


const VERBOSE = process.argv[2] === "--verbose";
const d = !VERBOSE ? function () {} : console.log;

const PATH_TO_CHECKPOINT_HASHES = path.resolve("../models/checkpoints/hashes.txt");
const PATH_TO_LORA_HASHES = path.resolve("../models/loras/hashes.txt");
const PATH_TO_EMBEDDINGS_HASHES = path.resolve("../models/embeddings/hashes.txt");
let curr = 0, tot = 0;

main();

/**
 * Waits 2 seconds
 * @returns {Promise<void>}
 */
function wait() {
  return new Promise((r) => {
    setTimeout(r, 2000);
  });
}

/**
 * Starts the main process.
 * - Prompts user for the folder path.
 * - Prompts user to add prefix or suffix.
 * Calls prepare one file for each png file
 */
async function main() {
  try {
    const { questionAsker, questionLoggers } = initUI({ questionCount: 2 });

    const files = await promptForWorkFolder(questionAsker, questionLoggers[0]);
    const prefixOrSuffix = await promptForPrefixOrSuffix(
      questionAsker,
      questionLoggers[1]
    );

    stopInteractivity();

    curr = 0;
    tot = files.length;
    for (const file of files) {
      await prepareOneFile(file, prefixOrSuffix);
      curr++;
      const left = `File ${curr}/${tot} `;
      rewriteConsole(`${left}${progressbar(curr, tot, left.length)}`);
    }
    const left = `File ${curr}/${tot} `;
    rewriteConsole(`${left}${progressbar(curr, tot, left.length)}`);
  } catch (err) {
    raiseError(err.message);
  }
}

/**
 * Gets folder path from user. Checks if exists. List files.
 * @param {PromptFn} questionAsker
 * @param {LogFn} questionLogger
 * @returns {Promise<String[]>}
 */
async function promptForWorkFolder(questionAsker, questionLogger) {
  const workpath =
    (await questionAsker("Path to workpath? (default: .) "));// || "test_civitai";
  // test dir
  if (workpath.includes("..")) {
    throw new Error("Path must be a child. Don't use '..'.");
  }
  try {
    await fs.lstat(path.resolve(workpath));
  } catch (err) {
    throw new Error("Could not access folder. " + err.message);
  }
  // list files
  const files = (await fs.readdir(workpath, { withFileTypes: true }))
    .filter((de) => de.name.slice(-4) === ".png")
    .map((de) => path.resolve(workpath, de.name));
  if (!files.length) throw new Error("Empty Folder.");
  // exists and not empty
  questionLogger(`Working in "${workpath}" with ${files.length} files.`);
  return files;
}

/**
 * Gets prefix or suffix parameter from user.
 * @param {PromptFn} questionAsker
 * @param {LogFn} questionLogger
 * @returns {Promise<"suffix"|"prefix">}
 */
async function promptForPrefixOrSuffix(questionAsker, questionLogger) {
  let prefixOrSuffix = await questionAsker(
    "Prefix (default) or Suffix? [p/s/prefix/suffix] "
  );
  prefixOrSuffix = !prefixOrSuffix
    ? "prefix"
    : ["s", "suffix"].includes(prefixOrSuffix.toLowerCase())
    ? "suffix"
    : "prefix";
  questionLogger(`Selected ${prefixOrSuffix} method.`);
  return /** @type {"suffix"|"prefix"} */ (prefixOrSuffix);
}

/**
 * Makes a jpeg copy of the source png. 
 * Strips metadata. 
 * Adds civitai compatible user comment.
 * @param {String} filepath Path to the file
 * @param {"prefix"|"suffix"} prefixOrSuffix Selected renaming method
 */
async function prepareOneFile(filepath, prefixOrSuffix) {
  const comment = await commentFromPng(filepath);
  
  // dest filename
  const destpath = filepath.replace(
    new RegExp("^(.+?)([^/\\\\]+)\\..+?$"),
    prefixOrSuffix === "suffix" ? "$1$2_civitai.jpeg" : "$1civitai_$2.jpeg"
  );

  // call imagemagick convert -strip to jpg
  await convertAndStrip(filepath, destpath);

  // call exiftool to add author and comment
  await addExifComment(destpath, comment);
}

/**
 * Builds a civitai compatible comment from the comfyui workflow embedded in the png tEXT.
 * The comment is loosely based on the workflow.
 * requirements:
 * - a "hashes.txt" file in the models/checkpoints folder
 *   format: checkpoint name:checkpoint hash:modelVersionId
 * - a "hashes.txt" file in the models/loras folder
 *   format: lora name:lora hash
 * - a "hashes.txt" file in the models/embeddings folder
 *   format: embedding file name:embedding hash
 * making a lot of assumptions
 * - first prompt is positive
 * - second prompt is negative
 * - all seeds are the same, keeping only the first
 * - lora and checkpoints hashes are saved in the models folders
 * other choices
 * - only keeping the first found checkpoint
 * - only keeping the parameters of the first found sampler
 * @param {String} filepath file path
 * @return {Promise<String>}
 */
async function commentFromPng(filepath) {
  // reading png as utf text to grab the workflow data
  let f = await fs.readFile(filepath, { encoding: "utf-8" });

  // attempt to extract prompts from the actual workflow json
  let p = "";
  let n = "";
  try {
    [p, n] = await findPromptsFromGUIWorkflow(f);
    d(`Found prompts in GUI workflow: ${!!p || !!n}`);
  } catch (err) {
    d(err.message);
    d(`Unable to find GUI workflow.`);
  }
  if (!p && !n)
    try {
      [p, n] = await findPromptsFromAPIWorkflow(f);
      d(`Found prompts in API workflow: ${!!p || !!n}`);
    } catch (err) {
      d(err.message);
      d(`Unable to find API workflow.`);
    }

  // data extraction from the raw data
  
  // regexps
  const ckptRegexp = /"(?:base_)?ckpt_name": ".*?([^"\\]+)\.[^"\\]+"/i;
  const lorasRegexp = /"lora_name": ".*?([^"\\]+)\.[^"\\]+"/gi;
  const promptsRegexp = /"(?:text|positive|negative|prompt)": "([^"]+)"/gi;
  const embeddingsRegexp = /embedding:(?:[^,\\]+\\+)*([^\\,]+),/gi;
  const seedsRegexp = /"(?:noise_)?seed": (\d+)/i;
  const heightRegexp = /"height": (\d+)/i;
  const widthRegexp = /"width": (\d+)/i;
  const stepsRegexp = /"steps": (\d+)/i;
  const cfgsRegexp = /"cfg": (\d+)/i;
  const samplersRegexp = /"sampler_name": "([^"]+)"/i;
  const schedulersRegexp = /"scheduler": "([^"]+)"/i;
  const denoisesRegexp = /"denoise": (\d+)/i;
  const vaesRegexp = /"vae_name": ".*?([^"\\]+)\.[^"]+"/i;
  const clipRegexp = /"stop_at_clip_layer": -(\d+)/i;

  // extractors
  /** @type {(reg:RegExp)=>string[]} */
  const extractMulti = (reg) => [...(new Set(
    f.match(reg)?.map((m) => m.replace(reg, "$1")) || []
    ))];
  /** @type {(reg:RegExp)=>string} */
  const extractOne = (reg) => f.match(reg)?.[0].replace(reg, "$1") || "";

  // values
  const ckpt = extractOne(ckptRegexp);
  const loras = extractMulti(lorasRegexp);
  const prompts = extractMulti(promptsRegexp);
  const embeddings = extractMulti(embeddingsRegexp);
  const seed = extractOne(seedsRegexp);
  const height = extractOne(heightRegexp);
  const width = extractOne(widthRegexp);
  const step = extractOne(stepsRegexp);
  const cfg = extractOne(cfgsRegexp);
  const sampler = extractOne(samplersRegexp);
  const scheduler = extractOne(schedulersRegexp);
  const denoise = extractOne(denoisesRegexp);
  const vae = extractOne(vaesRegexp);
  const clipSkip = extractOne(clipRegexp);

  // freeing space
  f = "";

  // hashes & versionIds
  let ckpt_hash = "";
  let ckpt_vId = "";
  const loraHashes = Array(loras.length);
  const embeddingHashes = Array(embeddings.length);
  try {
    // checkpoint
    if (ckpt) {
      const hashfile = await fs.readFile(
        PATH_TO_CHECKPOINT_HASHES,
        { encoding: "utf-8" }
      );
      const ckpt_entry = hashfile.match(new RegExp(ckpt + ":([^\r\n]+)", "i"))?.[0] || "";
      if(ckpt_entry) {
        let _;
        ([_,ckpt_hash,ckpt_vId]=ckpt_entry.split(":"));
      }
    }
    // lora lycoris
    if (loras?.length) {
      const hashfile = await fs.readFile(
        PATH_TO_LORA_HASHES,
        { encoding: "utf-8" }
      );
      for (let i = 0; i < loras.length; i++ ) {
        const loraname = loras[i];
        const hash =
          hashfile.match(new RegExp(loraname + ":(.+)", "i"))?.[0]?.replace(/^.+:/,"") || "";
        if (hash) loraHashes[i] = hash;
      }
    }
    // embeddings
    if(embeddings?.length){
      const hashfile = await fs.readFile(
        PATH_TO_EMBEDDINGS_HASHES,
        { encoding: "utf-8" }
      );
      for (let i = 0; i < embeddings.length; i++ ) {
        const embName = embeddings[i];
        const hash =
          hashfile.match(new RegExp(embName + ":(.+)", "i"))?.[0]?.replace(/^.+:/,"") || "";
        if (hash) embeddingHashes[i] = hash;
      }
    }
  } catch (err) {
    throw new Error("Unable to retrieve hashes. " + err.message);
  }

  // comment strings
  // end positive prompt with a line break.
  const pos = p ? p+"\n" : prompts?.[0] ? prompts?.[0] + "\n" : "";
  // end negative prompt with a line break.
  const neg = n ? "Negative prompt:  " +n+"\n" : prompts?.[1] ? "Negative prompt:  " + prompts?.[1] + "\n" : "";
  const s_steps = step ? `Steps: ${step}, ` : "";
  const s_sampler = sampler ? `Sampler: ${sampler}, ` : "";
  const s_cfg = cfg ? `CFG scale: ${cfg}, ` : "";
  const s_seed = seed ? `Seed: ${seed}, ` : "";
  const s_size = (width&&height) ? `Size: ${width}x${height}, ` : "";
  const s_clipSkip = clipSkip ? `Clip skip: ${clipSkip}, ` : "";
  const s_civRes = ckpt_vId ? "Civitai resources: "+JSON.stringify([{type:"checkpoint",modelVersionId:ckpt_vId}])+", " : "";
  // const s_model = ckpt ? `Model: ${ckpt}, ` : "";
  // const s_modelHash = ckpt_hash ? `Model Hash: ${ckpt_hash}, ` : "";
  const s_scheduler = scheduler ? `Scheduler: ${scheduler}, ` : "";
  const s_vae = vae ? `VAE: ${vae}, ` : "";
  const s_denoise = denoise ? `Denoise: ${denoise}, ` : ""
  const s_hashes = ckpt_hash || loraHashes.length || embeddingHashes.length
      ? `Hashes: {${(function () {
          const ks = [/* "model", */ ...loras, ...embeddings];
          const hs = [/* ckpt_hash, */ ...loraHashes, ...embeddingHashes];
          return ks
            // .reduce((str, k, i) => (k&&hs[i]) ? (str += `"${k}":"${hs[i]}", `) : str, "")
            .map((k, i) => (k&&hs[i]) ? (`"${k}":"${hs[i]}"`) : null, "")
            .filter(v => !!v)
            .join(`,`)
        })()}}`
      : "";
  const comment = pos+neg+s_steps+s_sampler+s_cfg+s_seed+s_size+s_clipSkip+s_civRes+/*s_model+s_modelHash+*/s_scheduler+s_vae+s_denoise+s_hashes;

return comment;
}

/**
 * Prints error and exits.
 * @param {String} text Text to print
 */
function raiseError(text) {
  rewriteConsole(`Stopped due to error at ${curr} of ${tot}`, true);
  rewriteConsole(text, true);
  wait().then(() => process.exit(1));
}

/**
 * Converts and strips an image
 * @param {String} from source path
 * @param {String} to destination path
 */
async function convertAndStrip(from, to) {
  await new Promise((res, rej) => {
    exec(`magick convert "${from}" -strip "${to}"`, (err, output) => {
      if (err) return rej(err);
      res(output);
    });
  });
}

/**
 * Converts and strips an image
 * @param {String} fpath source path
 * @param {String} comment comment to add
 */
async function addExifComment(fpath, comment) {
  // using a temp text file to circumvent problems with line breaks, quotation marks and other special characters.
  const tmpTxtFilePath = fpath.replace(/[^\\]+$/,"__temp_comment__.txt");
  // hack to get rid of the "ASCII" prefix on civitai, add unicode characters
  comment+= `, forceUtf8: 大家好`;
  await fs.writeFile(tmpTxtFilePath,comment,{encoding:"utf-8"});
  await new Promise((res, rej) => {
    exec(
      `exiftool -overwrite_original "-EXIF:UserComment<=${tmpTxtFilePath}" -charset=utf8 -F -ExifByteOrder=Little-endian "${fpath}"`,
      (err, output) => {
        if (err) return rej(err);
        res(output);
      }
    );
  });
  await fs.unlink(tmpTxtFilePath);
}





/////////////////////////////////////////////////////////
// Better workflow prompt extraction
/////////////////////////////////////////////////////////

// async function main() {
//   d(`Opening ${filepath}`);
//   const f = await fs.readFile(filepath, { encoding: "utf-8" });
//   let p = "";
//   let n = "";
//   try {
//     [p, n] = await findPromptsFromGUIWorkflow(f);
//     d(`Found prompts in GUI workflow: ${!!p || !!n}`);
//   } catch (err) {
//     console.log(err.message);
//     console.log(`Unable to find GUI workflow.`);
//   }
//   if (!p && !n)
//     try {
//       [p, n] = await findPromptsFromAPIWorkflow(f);
//       d(`Found prompts in API workflow: ${!!p || !!n}`);
//     } catch (err) {
//       console.log(err.message);
//       console.log(`Unable to find API workflow.`);
//     }
//   d(p);
//   d(n);
//   return [p, n];
// }

/**
 * @typedef GUINodeInputObject
 * @type {{[key:string]:string|[string,number]}}
 */
/**
 * @typedef GUINodeInputArray
 * @type {{name:string, type:string, link:number}[]}
 */
/**
 * @typedef GUINode
 * @type {Object}
 * @property {Number} id
 * @property {String} title
 * @property {String} type
 * @property {GUINodeInputObject|GUINodeInputArray} inputs
 * @property {string[]} [widgets_values]
 */

/**
 * @typedef GUIWorkflow
 * @type {Object}
 * @property {GUINode[]} nodes
 * @property {[number,number,number,number,number][]} links
 */

/**
 * @typedef {Object} APINodeMeta
 * @property {String} title
 */

/**
 * @typedef {Object} APINode
 * @property {APINodeMeta} _meta
 * @property {String} class_type
 * @property {GUINodeInputObject|GUINodeInputArray} inputs
 */

/**
 * @typedef {{[id:string]:APINode}} APIWorkflow
 *
 */

/**
 * Attempts to get the Positive & Negative prompts from a workflow
 * @param {String} filestring
 * @returns {Promise<string[]>}
 */
async function findPromptsFromGUIWorkflow(filestring) {
  const workflowRegExp =
    /EXtworkflow[^{]*(\{.+?\})[^}]*(?:EXtprompt|EXtCreationTime|IDAT)/;
  const workflowRaw = filestring.match(workflowRegExp)?.[1];
  if (!workflowRaw) throw new Error("Workflow not found.");
  // await fs.writeFile("___test.json", workflowRaw, {encoding:"utf-8"});
  /** @type {GUIWorkflow} */
  const workflow = JSON.parse(workflowRaw);
  if (typeof workflow !== "object")
    throw new TypeError("Expecting a JSON object.");
  if (!Array.isArray(workflow.nodes))
    throw new TypeError("Expecting a JSON Array.");
  // look for named CLIPTextEncoders
  const CLIPTextEncoders = workflow.nodes.filter(
    (node) => node.type === "CLIPTextEncode"
  );
  if (CLIPTextEncoders.length) {
    d(JSON.stringify(CLIPTextEncoders));
    d(`Searching by Clip Text Encode Title.`);
    let p = CLIPTextEncoders.filter(
      (node) => titlePositive(node) && hasWidgetValue(node)
    ).sort(sortGUINodesById);
    let n = CLIPTextEncoders.filter(
      (node) => titleNegative(node) && hasWidgetValue(node)
    ).sort(sortGUINodesById);
    if (p.length || n.length) {
      d(`Found by title.`);
    } else {
      d(`Not Found by title.`);
      d(`Searching from sampler.`);
      const samplers = workflow.nodes
        .filter(
          (node) =>
            /ksampler/i.test(
              node.type
            ) /*  && Array.isArray(node.inputs.positive) */
        )
        .sort(sortGUINodesById);
      if (samplers.length) {
        d(`Found ${samplers.length} sampler${samplers.length > 1 ? "s" : ""}`);
        for (let sampler of samplers) {
          d(`Searching from sampler id ${sampler.id}`);
          const pos_link_id = /** @type {GUINodeInputArray} */ (
            sampler.inputs
          ).find((i) => i.name === "positive")?.link;
          const neg_link_id = /** @type {GUINodeInputArray} */ (
            sampler.inputs
          ).find((i) => i.name === "negative")?.link;
          const [plid, pos_clip_id] =
            workflow.links.find(([id]) => id === pos_link_id) || [];
          const [nlid, neg_clip_id] =
            workflow.links.find(([id]) => id === neg_link_id) || [];
          d(pos_clip_id);
          d(neg_clip_id);
          if (!pos_clip_id || !neg_clip_id) continue;
          p = CLIPTextEncoders.filter((node) => node.id === pos_clip_id);
          n = CLIPTextEncoders.filter((node) => node.id === neg_clip_id);
          if (p.length || n.length) break;
        }
      } else {
        d(`Sampler not found.`);
      }
    }
    d(p);
    d(n);
    return [p[0]?.widgets_values?.[0] || "", n[0]?.widgets_values?.[0] || ""];
  }
  return [];
}

/**
 * Attempts to get the Positive & Negative prompts from a workflow
 * @param {String} filestring
 * @returns {Promise<string[]>}
 */
async function findPromptsFromAPIWorkflow(filestring) {
  const workflowRegExp =
    /EXtprompt[^{]*(\{.+?\})[^}]*(?:EXtworkflow|EXtCreationTime|IDAT)/;
  const workflowRaw = filestring.match(workflowRegExp)?.[1];
  if (!workflowRaw) throw new Error("Workflow not found.");
  await fs.writeFile("___test.json", workflowRaw, { encoding: "utf-8" });
  /** @type {APIWorkflow} */
  const workflow = JSON.parse(workflowRaw);
  if (typeof workflow !== "object")
    throw new TypeError("Expecting a JSON object.");
  // look for named CLIPTextEncoders
  const CLIPTextEncoders = Object.entries(workflow).filter(
    ([id, node]) => node.class_type === "CLIPTextEncode"
  );
  if (CLIPTextEncoders.length) {
    d(JSON.stringify(CLIPTextEncoders));
    const p = CLIPTextEncoders.filter(([id, node]) => titlePositive(node))
      .sort(sortAPINodesById)
      .map(FindPromptAPI(workflow))
      .filter((s) => !!s);
    const n = CLIPTextEncoders.filter(([id, node]) => titleNegative(node))
      .sort(sortAPINodesById)
      .map(FindPromptAPI(workflow))
      .filter((s) => !!s);

    return [p[0] || "", n[0] || ""];
  }
  return [];
}

/**
 * test for node title to include "positive"
 * @param {GUINode|APINode} node
 * @returns {Boolean}
 */
function titlePositive(node) {
  const reg = /positive/i;
  const title =
    /** @type {GUINode} */ (node).title ||
    /** @type {APINode} */ (node)._meta?.title ||
    "";
  return reg.test(title);
}
/**
 * test for node title to include "negative"
 * @param {GUINode|APINode} node
 * @returns {Boolean}
 */
function titleNegative(node) {
  const reg = /negative/i;
  const title =
    /** @type {GUINode} */ (node).title ||
    /** @type {APINode} */ (node)._meta?.title ||
    "";
  return reg.test(title);
}
/**
 * Sorting GUI workflow entries by id
 * @param {GUINode} a
 * @param {GUINode} b
 * @returns {Number}
 */
function sortGUINodesById(a, b) {
  return a.id - b.id;
}
/**
 * Sorting API workflow entries by id
 * @param {[string,any]} a
 * @param {[string,any]} b
 * @returns {Number}
 */
function sortAPINodesById([aid, _a], [bid, _b]) {
  return parseInt(aid) - parseInt(bid);
}
/**
 * tests for GUI clip text encode to have a widget value
 * @param {GUINode} node
 * @returns {Boolean}
 */
function hasWidgetValue(node) {
  return (node.widgets_values?.[0] || "") !== "";
}
/**
 * @typedef apiNode
 * @type {Object}
 * @property {Object} [inputs]
 * @property {string|string[]} [inputs.text]
 * @property {string|string[]} [inputs.strings]
 */
/**
 * Attempts to find the text value of a clip text encode
 * by navigating the input tree.
 * @param {{[key:string]:apiNode}} workflow
 * @param {apiNode} node
 * @returns {String}
 */
function findPromptRecursiveAPI(workflow, node) {
  if (typeof node?.inputs?.text === "string") return node.inputs.text;
  if (typeof node?.inputs?.strings === "string") return node.inputs.strings;
  const target_id = node.inputs?.text?.[0];
  if (typeof target_id !== "string" || !target_id)
    throw new Error("No text parent found.");
  d(target_id);
  const _node = workflow[target_id];
  if (!_node)
    throw new Error("Parent text node not found. (id: " + target_id + ")");
  return findPromptRecursiveAPI(workflow, _node);
}
function FindPromptAPI(workflow) {
  return function ([id, node]) {
    d(id);
    try {
      return findPromptRecursiveAPI(workflow, node);
    } catch (err) {
      d("No value found for node " + id);
      d(err.message);
      return false;
    }
  };
}