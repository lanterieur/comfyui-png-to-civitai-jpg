// @ts-check
/**
 * @typedef LogFn
 * @type {(text:string)=>void}
 */
/**
 * @typedef PromptFn
 * @type {(text:string)=>Promise<string>}
 */
/**
 * @typedef QuestionLoggers
 * @type {LogFn[]}
 * */
/**
 * @typedef UiFns
 * @type {Object}
 * @property {PromptFn} questionAsker
 * @property {QuestionLoggers} questionLoggers
 */
const EventEmitter = require("events");

const readline = require("readline").createInterface({
  input: process.stdin,
  output: process.stdout,
});

let lineOffset = 0;
module.exports = {
  readline,
  logAt,
  initUI,
  rewriteConsole,
  stopInteractivity,
  progressbar
};

const stdQueue = new EventEmitter();
stdQueue.on("write", onWriteHandler);

/**
 * Handles std write queue events
 * @param {Number} x col
 * @param {Number} y row
 * @param {String} text text to write on stdout
 * @param {Boolean} [clear] flag to clear the line
 */
function onWriteHandler(x, y, text, clear = false) {
  process.stdout.cursorTo(x, y);
  if (clear) process.stdout.clearLine(0);
  process.stdout.write(text);
}

/**
 * Queues a std write request
 * @param {Number} x col
 * @param {Number} y row
 * @param {String} text text to write on stdout
 * @param {Boolean} [clear] flag to clear the line
 */
function logAt(x, y, text, clear = false) {
  stdQueue.emit("write", x, y, text, clear);
}

/**
 * Rewrites the last std out line.
 * @param {String} text text to print
 * @param {Boolean} [permanent] flag to keep line permanent and add a new last line
 */
function rewriteConsole(text, permanent = false) {
  logAt(0, 9 + lineOffset, text, true);
  if (permanent) lineOffset++;
}

/**
 * Draws the initial User Interface
 * @param {Object} [config] config object
 * @param {Number} [config.questionCount] Number of questions
 * @returns {UiFns}
 */
function initUI(config) {
  const questionCount = config?.questionCount || 0;

  const uiHeight =
    2 + // top and bottom margins
    2 + // title and subtitle
    (questionCount ? 1 : 0) + // padding if question(s)
    questionCount; // one line per question

  // clear stdout
  process.stdout.cursorTo(0, 0);
  process.stdout.clearScreenDown();

  // draws left and right borders
  for (let r = 0; r < uiHeight; r++) {
    process.stdout.cursorTo(0, r);
    process.stdout.write("║");
    process.stdout.cursorTo(process.stdout.columns - 1, r);
    process.stdout.write("║");
  }
  // draws top and bottom borders
  const uiWidth = process.stdout.columns;
	const horCharTop = r => r === 0 ? "╔" : r < uiWidth-1 ? "═" : "╗";
	const horCharBtm = r => r === 0 ? "╚" : r < uiWidth-1 ? "═" : "╝";
  for (let c = 0; c < uiWidth; c++) {
    process.stdout.cursorTo(c, 0);
    process.stdout.write(horCharTop(c));
    process.stdout.cursorTo(c, uiHeight);
    process.stdout.write(horCharBtm(c));
  }
  // prints title and subtitle
  process.stdout.cursorTo(2, 1);
  process.stdout.write("Comfyui To Civitai Image Preparation Tool");
  process.stdout.cursorTo(10, 2);
  process.stdout.write("Turns png to jpeg with civitai generation data.");
  // prints question and prepare answer loggers
  /** @type {QuestionLoggers} */
  const questionLoggers = [];
  if (questionCount)
    for (let i = 0; i < questionCount; i++) {
      const y = 4 + i;
      process.stdout.cursorTo(2, y);
      process.stdout.write(`Q${i + 1}:`);
      questionLoggers.push((text) => logAt(8, y, text));
    }
  // prints log header
  process.stdout.cursorTo(2, uiHeight + 1);
  process.stdout.write("Logs\n");

  // question asker
  /** @type {PromptFn} */
  const questionAsker = function (prompt) {
    return new Promise((r, rj) => {
      try {
        process.stdout.cursorTo(0, uiHeight + 2);
        readline.question(prompt, r);
      } catch (err) {
        rj(err);
      }
    });
  };

  return { questionAsker, questionLoggers };
}

/**
 * Closes readline interface
 */
function stopInteractivity() {
  readline.close();
}

/**
 * Returns a progress string.
 * " 87% │████████  │"
 * @param {Number} cur current progress
 * @param {Number} tot total
 * @param {Number} xoffset left offset
 * @returns {String}
 */
function progressbar(cur,tot,xoffset){
  const available_x = process.stdout.columns - xoffset;
  const barLn = available_x - 5 - 2;
  const pct = cur/tot;
  const pct_str = `${(pct*100).toFixed(0).padStart(3," ")}% `;
  const done_str = "".padEnd(Math.floor(barLn*pct),"█");
  const bar = `│${done_str}${"".padEnd(barLn-done_str.length," ")}│`;
  return pct_str + bar;
}